<?php
$requireLogIn = false;
require_once 'bootstrap.php';
$level = getAccountLevel();
if ($level == 'organizator') {
    header("Location: ./index_organizator.php");
}

//Base Template
$templateParams["title"] = "Termini d'uso";
$templateParams["js"] = array(
    "http://zxue.me/interactive2/project3/js/archive.js",
    "//code.jquery.com/ui/1.11.4/jquery-ui.js",
    "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"
);
$templateParams["css"] = array(
    "./css/base_term.css",
    "http://zxue.me/interactive2/project3/css/archive.css",
    "http://zxue.me/interactive2/project3/css/layout.css",
    "https://fonts.googleapis.com/css?family=Montserrat:400,700"
);
$templateParams["page"] = array("term_and_conditionTemplate.php");

require 'template/base.php';