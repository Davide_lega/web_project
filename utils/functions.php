<?php
function sec_session_start()
{
    $session_name = 'sec_session_id';
    $secure = false;
    $httponly = true;
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name);
    session_start();
    session_regenerate_id();
}
function redirect($url)
{
    header('Location: ' . $url);
    exit();
}
function checkAccountLevel($level){
    if (isset($_SESSION['level'])) {
        if ($_SESSION['level'] != $level) {
            header("Location: ./index.php?error=0");
        }
    } else {
        header('Location: ./log_in.php?error=4');
    }
}
function getAccountLevel(){
    if(isset($_SESSION['level'])){
        return $_SESSION['level'];
    } else {
        return false;
    }
}
function uploadImage()
{
    $target_dir = "../upload/img/";
    $target_file = $target_dir . basename($_FILES["newImage"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $check = getimagesize($_FILES["newImage"]["tmp_name"]);
    //print_r($check);

    if ($check !== false) { //Check if is an image
        if (!file_exists($target_file)) { //Check if doesn't already exist
            if ($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif") { //Format check
                if (move_uploaded_file($_FILES["newImage"]["tmp_name"], $target_file)) { //Move it
                    return true;
                }
            }
        }
    }
    return false;
}
