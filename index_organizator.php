<?php
$requireLogIn = true;
require_once 'bootstrap.php';
checkAccountLevel('organizator');

//Base Template
$templateParams["title"] = "Home - Organizzator";
$templateParams["js"] = array("./js/indexOrganizator.js");
$templateParams["css"] = array("./css/base_org.css");
$templateParams["page"] = array("./template/organizzator.php");
$templateParams["myEvents"] = $dbh->getOrganizatorEvents($_SESSION['mail']);

require 'template/base_org.php';
