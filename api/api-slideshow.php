<?php
$requireLogIn = false;
require_once '../bootstrap.php';

$events = $dbh->getRandomEvents($_GET['numberOfElement']);
for ($i = 0; $i < count($events); $i++) {
    $events[$i]["image"] = UPLOAD_DIR . $events[$i]["image"];
}
header("Content-Type: application/json");
echo json_encode($events, JSON_FORCE_OBJECT);
