<?php
$requireLogIn = false;
require_once '../bootstrap.php';

$events = $dbh->getEventByAllSelector($_GET['selectorArray']);
header("Content-Type: application/json");
echo json_encode($events, JSON_FORCE_OBJECT);
?>