<?php
$requireLogIn = true;
require_once '../bootstrap.php';

if(isset($_SESSION["mail"])){ //GET USER EVENT FROM DB
    $events = $dbh->getBuyedEvents($_SESSION["mail"]);
} else {
    $events = "";
}
header("Content-Type: application/json");
echo json_encode($events, JSON_FORCE_OBJECT);
?>