<?php
$requireLogIn = true;
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $events = $dbh->getCartEvents($_SESSION['mail']);
    echo json_encode($events, JSON_FORCE_OBJECT);
}
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if ($_POST["op"] == "+") {
        $dbh->updatePlusInCart($_SESSION["mail"], $_POST["id"]);
    } else if ($_POST["op"] == "-") {
        $dbh->updateMinusInCart($_SESSION["mail"], $_POST["id"]); 
    } elseif ($_POST["op"] == "delete") {
        $dbh->deleteFromCART($_SESSION["mail"], $_POST["id_event"]);
    }
}