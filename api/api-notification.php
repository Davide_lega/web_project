<?php
$requireLogIn = true;
require_once '../bootstrap.php';
if ($_SERVER['REQUEST_METHOD']=="GET") {
    $notification = $dbh->getUserNotifications($_SESSION["mail"]);
    echo json_encode($notification, JSON_FORCE_OBJECT);
}
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if ($_POST["op"] == "read") {
        $dbh->updateNotificationToRead($_POST["id_notification"]);
    } elseif ($_POST["op"] == "delete") {
        $dbh->deleteNOTIFICATIONS($_POST["id_notification"]);
    }
}
?>