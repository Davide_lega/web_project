<?php
$requireLogIn = false;
require_once '../bootstrap.php';
$temp = $dbh->getMinPrice();
$values["MinPrice"] = $temp[0]["MinPrice"];
$temp = $dbh->getMaxPrice();
$values["MaxPrice"] = $temp[0]["MaxPrice"];

header("Content-Type: application/json");
echo json_encode($values, JSON_FORCE_OBJECT);
