<?php
$requireLogIn = true;
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") { //CHECK IF IS POSSIBLE TO ADD ANOTHER TICKET
    if ($dbh->getEventById($_GET["event"]["id_event"])[0]["quantity"] >= $_GET["event"]["quantity"] + 1) {
        $inc = true;
    } else {
        $inc = false;
    }
    echo json_encode($inc);
}
