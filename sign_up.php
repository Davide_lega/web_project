<?php
$requireLogIn = false;
require_once 'bootstrap.php';

$templateParams["title"] = "Sign In";
$templateParams["js"] = array("./js/sha512.js", "./js/form_login.js");
$templateParams["css"] = array("./css/log_in.css");

//ERROR MANAGEMENT
if (isset($_GET["error"])) {
    if ($_GET["error"] == 1) {
        $templateParams["error"] = "La mail è già stata usata";
    } else if ($_GET["error"] == 2) {
        $templateParams["error"] = "Inserimento nel database fallito";
    }
}

require 'template/sign_upT.php';
