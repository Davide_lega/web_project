<?php
$requireLogIn = true;
require_once 'bootstrap.php';

$templateParams["css"] = array("./css/base.css", "./css/ticket.css");
$templateParams["page"] = array("./template/ticket_template.php");

require 'template/base.php';
