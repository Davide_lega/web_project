<?php
require_once "utils/functions.php";
sec_session_start();
require_once "db/database.php";
$dbh = new DatabaseManager("localhost", "root", "", "web");
define("UPLOAD_DIR", "./upload/img/");

if (isset($requireLogIn)) { //LOGIN CHECK
    if ($requireLogIn) {
        if (!$dbh->is_logged()) {
            redirect("log_in.php");
        }
    }
} else {
    redirect("log_in.php");
}
