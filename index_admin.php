<?php
$requireLogIn = false;
require_once 'bootstrap.php';
checkAccountLevel('admin');

//Base Template
$templateParams["title"] = "Home - Admin";
$templateParams["js"] = array("");
$templateParams["css"] = array("./css/base_org.css","./css/admin.css");
$templateParams["page"] = array("./template/admin_template.php");

require 'template/base_admin.php';
