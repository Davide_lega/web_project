<?php
$requireLogIn = false;
require_once 'bootstrap.php';
$level = getAccountLevel();
if ($level == 'organizator') {
    header("Location: ./index_organizator.php");
}

//Base Template
$templateParams["title"] = "Woodland - Home";
$templateParams["js"] = array("./js/slideshow.js", "./js/search_index.js", "//code.jquery.com/ui/1.11.4/jquery-ui.js");
$templateParams["css"] = array("./css/base.css", "./css/index.css", "//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
$templateParams["page"] = array("search_form.php", "home.php");
$randomEvents = $dbh->getRandomEvents(6);
$templateParams["random_events1"] = array_slice($randomEvents, 0, 3);
$templateParams["random_events2"] = array_slice($randomEvents, 3);

if ($dbh->login_check()) {
    $templateParams["user_event"] = $dbh->getBuyedEventsByDate($_SESSION["mail"]);
}
require 'template/base.php';
