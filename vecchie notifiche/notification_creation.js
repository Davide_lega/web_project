$(document).ready(function() {
	$(".submit").click(function(event) {
		error = false;
		event.preventDefault();
		var i=0;
		$("#form :input").each(function() { //CHECK IF ALL INPUT HAVE VALUE
			i++;
			$(this).css("border-color", "#707070");
			if ($(this).val() == "") {
				$(this).css("border-color", "red");
				error = true;
			}
		});
		if(error === false){
			$("#form").submit();
		}
	});
});
