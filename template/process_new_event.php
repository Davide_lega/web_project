<?php
$requireLogIn = false;
require_once '../bootstrap.php';

if (uploadImage()) {
    if($dbh->insertEvent(
        $_POST["quantity"],
        $_POST["price"],
        $_POST["end_date_time"],
        $_POST["start_date_time"],
        $_FILES["newImage"]["name"],
        $_POST["description"],
        $_POST["title"],
        $_POST["city"],
        $_POST["category"],
        $_SESSION["mail"]
    )){
        header('Location: ../index_organizator.php');
    } else {
        header('Location: ../index_organizator.php?error_new_process=1');
    }
} else {
    header('Location: ../event_creation.php?error_image=1');
}
