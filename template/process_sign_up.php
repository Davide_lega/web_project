<?php
$requireLogIn = false;
require_once '../bootstrap.php';

if (empty($dbh->userExist($_POST['mail']))) { //CHECK IF THE USER ALREADY EXIST
    if ($dbh->new_User( //INSERT USER
        $_POST['mail'],
        $_POST['passwordCript'],
        $_POST['name'],
        $_POST['surname'],
        $_POST['postcode'],
        $_POST['country'],
        $_POST['account_type']
    )) { // INSERT SUCCESSFULL
        if ($dbh->login($_POST['mail'], $_POST['passwordCript'])) { //LOGIN USER
            if($_SESSION['level'] == 'organizator'){
                header('Location: ../index_organizator.php');
            } else {
                header('Location: ../index.php');
            }
        } else { //FAILED LOGIN FOR SOME REASON
            header('Location: ../log_in.php?error=3');
        }
    } else { //FAILED TO INSERT NEW USER
        header('Location: ../sign_up.php?error=2');
    }
} else {
    header('Location: ../sign_up.php?error=1');
}
