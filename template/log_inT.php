<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://code.iconify.design/1/1.0.4/iconify.min.js"></script>
    <title><?php echo $templateParams["title"] ?></title>
    <?php
    if (isset($templateParams["js"])) : //JS FUNCTIONS
        foreach ($templateParams["js"] as $script) :
    ?>
            <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php
        endforeach;
    endif;
    if (isset($templateParams["css"])) : //CSS STYLE
        foreach ($templateParams["css"] as $style) :
        ?>
            <link rel="stylesheet" href=<?php echo $style; ?> />
    <?php
        endforeach;
    endif;
    ?>
</head>

<body>
    <header>
        <div class=".headerDiv">
            <h1 class="title">Accedi</h1>
            <?php if (isset($templateParams["error"])) : ?>
                <label class="errorLabel"><?php echo $templateParams["error"]; ?></label>
            <?php endif; ?>
            <a href="./index.php" class="closeButton"><span class="iconify" data-icon="ant-design:home-outlined" data-inline="false"></span></a>
        </div>
        <p class="askRegistration">Sei nuovo su Wooderland?<a href="./sign_up.php">Registrati ora!</a></p>
    </header>
    <form action="./template/process_login.php" method="post" name="login_form" class="login_form" id="form">
        <label class="inputLabel" for="mailInput">Indirizzo Email:</label>
        <input class="mailInput inputBase" type="text" name="mail" id="mail" autofocus /><br />
        <label class="inputLabel" for="passwordInput">Password:</label>
        <input class="passwordInput inputBase" type="password" name="password" id="password" /><br />
        <input type="button" class="submitButton" value="Login" />
    </form>
</body>

</html>