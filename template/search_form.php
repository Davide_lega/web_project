<form class="searchForm" action="get">
    <!--TODO citta`date categorie prezzo-->


    <div id="formCity" type="text" class="multiInput">
        <label id="labelCity" for="formCity">Città:</label>
    </div>
    <div id="divDateS">
        <label id="labelDate" for="formDateS">Dal:</label>
        <input id="formDateS" type="date" class="formDateS" value="<?php echo date('Y-m-d') ?>"></input>
    </div>
    <div id="divDateE">
        <label id="labelDate" for="formDateE">Al:</label>
        <input id="formDateE" type="date" class="formDateE" value="<?php echo date('Y-m-d', strtotime("+7 day")); ?>"></input>
    </div>

    <p>
        <label for="amount">Prezzo:</label>
        <input type="text" id="amount" readonly style="border:0; color:#219354; font-weight:bold;"></input>
    </p><br>
    <div id="slider-range"></div><br>
    <div id="formCategory" class="searchBar multiInput" type="text" id="bar">
        <label id="labelCategory" for="formCategory">Categoria:</label>
    </div>
    <button class="searchButton" type="submit">Cerca</button>
</form>