<?php
$requireLogIn = false;
require_once '../bootstrap.php';

if (isset($_POST['mail'], $_POST['passwordCript'])) { //CHECK VARIABLE
    $mail = $_POST['mail'];
    $password = $_POST['passwordCript'];
    if ($dbh->login($mail, $password)) { //LOGIN SUCCESSFULL
        
        
        header('Location: ../index.php');
        $dbh->login_check();
        if ($_SESSION['level'] == 'admin') {
            header("Location: ../index_admin.php");
        } else if ($_SESSION['level'] == 'organizator') {
            header("Location: ../index_organizator.php");
        } else if ($_SESSION['level'] == 'client') {
            header("Location: ../index.php");
        }
    } else { //LOGIN FAILED
        header('Location: ../log_in.php?error=1');
    }
} else { //SOMETHING WENT REALLY WRONG
    header('Location: ../log_in.php?error=2');
}
