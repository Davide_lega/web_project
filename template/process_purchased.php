<?php
$requireLogIn = true;
require_once '../bootstrap.php';

$events = $dbh->getUserCart($_SESSION["mail"]);
foreach ($events as $elem) {
    global $dbh;
    $remaning_seats = $dbh->getEventById($elem['id_event'])["0"]["quantity"];
    if ($remaning_seats >= $elem["quantity"]) {
        $ElemTicket = $dbh->getTicket($_SESSION["mail"],$elem["id_event"]);
        $dbh->updateEventQuantity($elem["id_event"], $remaning_seats - $elem["quantity"]);
        $dbh->deleteFromCART($_SESSION["mail"], $elem["id_event"]);
        if(empty($ElemTicket)){
            $dbh->insertTicket($_SESSION["mail"], $elem["id_event"], $elem["quantity"]);
        }
        else{
            $newQuantity = $elem["quantity"] + $ElemTicket["0"]["quantity"];
            $dbh->updateTicketQuantity($elem["id_event"] ,$_SESSION["mail"] ,$newQuantity);
        }
        header('Location: ../ticket.php');
    }
    else{
    header("Location: ../cart.php?error=1");
    }
}
