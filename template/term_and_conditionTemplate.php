<div class="content-a">
    <div class="letters">
        <!--IF-->
        <div class="letter" data-character="i">
            <div class="letter">
                <div class="I">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="f">
            <div class="letter">
                <div class="F">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character=" ">
            <div class="letter">
                <div class="space"></div>
            </div>
        </div>
        <!--YOU-->
        <div class="letter" data-character="y">
            <div class="letter">
                <div class="Y">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="o">
            <div class="letter">
                <div class="O">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="u">
            <div class="letter">
                <div class="U">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character=" ">
            <div class="letter">
                <div class="space"></div>
            </div>
        </div>
        <!--CAN-->
        <div class="letter" data-character="c">
            <div class="letter">
                <div class="C">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="a">
            <div class="letter">
                <div class="A">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="n">
            <div class="letter">
                <div class="N">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="letters">
        <!--READ-->
        <div class="letter" data-character="r">
            <div class="letter">
                <div class="R">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="e">
            <div class="letter">
                <div class="E">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="a">
            <div class="letter">
                <div class="A">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="d">
            <div class="letter">
                <div class="D">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character=" ">
            <div class="letter">
                <div class="space"></div>
            </div>
        </div>
        <!--THIS-->
        <div class="letter" data-character="t">
            <div class="letter">
                <div class="T">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="h">
            <div class="letter">
                <div class="H">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="i">
            <div class="letter">
                <div class="I">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="s">
            <div class="letter">
                <div class="S">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character=" ">
            <div class="letter">
                <div class="space"></div>
            </div>
        </div>
        <!--YOU-->
        <div class="letter" data-character="y">
            <div class="letter">
                <div class="Y">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="o">
            <div class="letter">
                <div class="O">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="u">
            <div class="letter">
                <div class="U">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="letters">
        <!--ARE-->
        <div class="letter" data-character="a">
            <div class="letter">
                <div class="A">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="r">
            <div class="letter">
                <div class="R">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="e">
            <div class="letter">
                <div class="E">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character=" ">
            <div class="letter">
                <div class="space"></div>
            </div>
        </div>
        <!--NOT-->
        <div class="letter" data-character="n">
            <div class="letter">
                <div class="N">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="o">
            <div class="letter">
                <div class="O">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="t">
            <div class="letter">
                <div class="T">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character=" ">
            <div class="letter">
                <div class="space"></div>
            </div>
        </div>
        <!--BLIND-->
        <div class="letter" data-character="b">
            <div class="letter">
                <div class="B">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="l">
            <div class="letter">
                <div class="L">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="i">
            <div class="letter">
                <div class="I">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="n">
            <div class="letter">
                <div class="N">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
        <div class="letter" data-character="d">
            <div class="letter">
                <div class="D">
                    <div class="eye"></div>
                </div>
            </div>
        </div>
    </div>