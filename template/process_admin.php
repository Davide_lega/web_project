<?php
$requireLogIn = true;
require_once '../bootstrap.php';

if($_POST["operation"] == "UserD"){
    if($dbh->userExist($_POST["Data"])[0]!==null){

        foreach ($dbh->getOrganizatorEvents($_POST["Data"])["id_event"] as $EventId){
            EventDeleteAndNotify($EventId);
        }

        if($dbh->deleteUSER($_POST["Data"])){
            header("Location: ../index_admin.php");
        }

    }
    else{
        header("Location: ../index_admin.php?result=1");
    }
    
}

elseif($_POST["operation"] == "EventD"){
    EventDeleteAndNotify((int)$_POST["Data"]);
}

elseif($_POST["operation"] == "EventU"){
    $temp = (int)$_POST["Data"];
    header("Location: ../event_update.php?id=$temp");
}

function EventDeleteAndNotify($id){

    global $dbh;
    $Users = $dbh->getUsersToNotify((int)$_POST["Data"]);
    $Etitle = $dbh->getEventById($id)[0]["title"];
    $Start_date =$dbh->getEventById($id)[0]["start_date_time"];
    $End_date = $dbh->getEventById($id)[0]["end_date_time"];
    $Location = $dbh->getEventById($id)[0]["city"];
    $_SESSION["Users"] = $Users;
    $_SESSION["Etitle"] = $Etitle;
    $_SESSION["Start_date"] = $Start_date;
    $_SESSION["End_date"] = $End_date;
    $_SESSION["Location"] = $Location;

    if($dbh->deleteEVENT((int)$_POST["Data"])){
        $temp =(int)$_POST["Data"];
        $NoType = "EventDelete";
        header("Location: ./process_new_notification.php?NoType=$NoType");
    }
    else{
         header("Location: ../index_admin.php?result=2");
    }

}