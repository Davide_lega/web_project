<section>
<form class="AdminManagment" action="./template/process_admin.php" method="post">
  <p>Seleziona l'operazione :</p>
  <input type="radio" id="UserD" name="operation" value="UserD" checked>
  <label for="UserD"> Cancella utente (per mail)</label>
  <input type="radio" id="EventD" name="operation" value="EventD">
  <label for="EventD">Cancella evento (per ID)</label>
  <input type="radio" id="EventU" name="operation" value="EventU">
  <label for="EventU">Aggiorna evento (per ID)</label>
  <input type="text" id="Data" name="Data" required>
  <input type="submit" value="Conferma" name="submit" class="submit"></input>

  <?php 
  if(isset($_GET["result"])):
    if($_GET["result"] == 1):
    ?>
      <p class="UserDNotOk"> Cancellazione dell'utente non avvenuta correttamente</p>
    <?php
    endif;
    if($_GET["result"] == 2):
      ?>
        <p class="EventNotOK"> Cancellazione dell'evento non avvenuta correttamente</p>
    <?php
    endif;
  endif;
  ?>

</form>
</section>