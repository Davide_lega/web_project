<?php
$requireLogIn = false;
require_once '../bootstrap.php';

//if there is a new image upload it, else update only the others information
if ($_POST["imageRadio"] == "new") {
    if (uploadImage()) {
        if ($dbh->updateEvent(
            $_POST['title'],
            $_POST['city'],
            $_POST['quantity'],
            $_POST['price'],
            $_POST['endDate'],
            $_POST['startDate'],
            $_FILES['newImage']['name'],
            $_POST['desc'],
            $_POST['id_event']
        )) {
            $_SESSION["Etitle"] = $_POST["title"];
            $_SESSION["Users"] = $dbh->getUsersToNotify($_POST['id_event']);
            header("Location: ./process_new_notification.php?NoType=EventUpdate");
        } else {
            $level = getAccountLevel();
            header("Location: ../index_$level.php?error=2");
        }
    } else {
        $level = getAccountLevel();
        header("Location: ../index_$level.php?error=3");
    }
} else {
    if ($dbh->updateEventWithoutImage(
        $_POST['title'],
        $_POST['city'],
        $_POST['quantity'],
        $_POST['price'],
        $_POST['endDate'],
        $_POST['startDate'],
        $_POST['desc'],
        $_POST['id_event']
    )) {
        $_SESSION["Etitle"] = $_POST["title"];
        $_SESSION["Users"] = $dbh->getUsersToNotify($_POST['id_event']);
        header("Location: ./process_new_notification.php?NoType=EventUpdate");
    } else {
        $level = getAccountLevel();
        header("Location: ../index_$level.php?error=3");
    }
}
