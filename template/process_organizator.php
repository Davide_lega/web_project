<?php
$requireLogIn = true;
require_once '../bootstrap.php';

$Users = $dbh->getUsersToNotify($_GET["id"]);
$Etitle = $_GET["title"];
$Start_date = $_GET["start_date_time"];
$End_date = $_GET["end_date_time"];
$Location = $_GET["city"];
$_SESSION["Users"] = $Users;
$_SESSION["Etitle"] = $Etitle;
$_SESSION["Start_date"] = $Start_date;
$_SESSION["End_date"] = $End_date;
$_SESSION["Location"] = $Location;

if ($dbh->deleteEVENT($_GET["id"])) {
    $NoType = "EventDelete";
    header("Location: ../template/process_new_notification.php?NoType=$NoType");
} else {
    header("Location: ../index_organizator.php?result=2");
}
