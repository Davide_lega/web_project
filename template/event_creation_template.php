<form id ="form" action="./template/process_new_event.php" method="post" enctype="multipart/form-data">
    <div class ="imagine">
        <p>Selezione l'immagine dell evento :</p>
        <input type="file" name="newImage" id="fileToUpload"></input>
    </div>
    <div class="titleEv">
        <p>Titolo :</p>
        <input type="text" value="" name="title"></input>
    </div>
    <div class="desc">
        <p>Descrizione Evento :</p>
        <textarea name="description" class="descrizione"></textarea>
    </div>
    <div class="dates">
        <p>Data di inizio :</p>
        <input type="date" value="" name="start_date_time" class="start_date_time" min="2020-01-01" max="3000-01-01"></input>
        <p>Data di fine </p>
        <input type="date" value="" name="end_date_time" class="end_date_time" min="2020-01-01"  max="3000-01-01"></input>
    </div>
    <div>
        <p>Categoria :</p>
        <input type="text" value="" name="category"></input>
    </div>
    <div>
        <p>Città :</p>
        <input type="text" value="" name="city"></input>
    </div>
    <div class="price&quantity">
        <p>Prezzo :</p>
        <input type="number" value="" name="price" min="0"></input>
        <p>Posti disponibili :</p>
        <input type="number" value="" name="quantity" min="0"></input>   
    </div>
    <div class="submitB">
        <input type="submit" onclick="shish()" method="post" value="Conferma" name="submit" class="submit"></input>
    </div>
</form>
 <!--LOGIN CHECK-->