<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://code.iconify.design/1/1.0.4/iconify.min.js"></script>
    <title><?php echo $templateParams["title"] ?></title>
    <?php
    if (isset($templateParams["js"])) : //JS FUNCTIONS
        foreach ($templateParams["js"] as $script) :
    ?>
            <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php
        endforeach;
    endif;
    if (isset($templateParams["css"])) : //CSS STYLE
        foreach ($templateParams["css"] as $style) :
        ?>
            <link rel="stylesheet" href=<?php echo $style; ?> />
    <?php
        endforeach;
    endif;
    ?>
</head>

<body>
    <header>
        <div class=".headerDiv">
            <h1 class="title">Accedi</h1>
            <?php if (isset($templateParams["error"])) : ?>
                <label class="errorLabel"><?php echo $templateParams["error"]; ?></label>
            <?php endif; ?>
            <a href="./index.php" class="closeButton"><span class="iconify" data-icon="ant-design:home-outlined" data-inline="false"></span></a>
        </div>
    </header>
    <br>
    <br>
    <form action="./template/process_sign_up.php" method="post" id="form">
        <div class="upperDiv">
            <label for="mail" class="mailLabel">Indirizzo Email: <input class="mailInput inputBase" type="text" name="mail" id="mail" value="" autofocus /></label>
            <label for="password" class="passwordLabel">Password: <input class="passwordInput inputBase" type="password" value="" name="password" id="password" /></label>
            <label for="name" class="nameLabel">Nome: <input class="nameInput inputBase" type="text" name="name" id="name" value="" /></label>
            <label for="surname" class="surnameLabel">Cognome: <input class="surnameInput inputBase" type="text" value="" name="surname" id="surname" /></label>
            <label for="postcode" class="postcodeLabel">Codice postale/Zip: <input class="postcodeInput inputBase" value="" type="number" name="postcode" id="postcode" /></label>
            <div id="country" class="countryDiv">
                Paese di residenza:
                <select name="country" id="country" class="countrySelect  inputBase">
                    <option value="Italia">Italia</option>
                    <option value="Altro..">Altro..</option>
                </select>
            </div>
        </div>
        <div id="userType" class="typeDiv">
            <label class="typeMessage">Scegli il tipo di account:</label><br />
            <input type="radio" class="radioInput" name="account_type" value="client" id="client" checked="checked">
            <label for="Client">Cliente</label>
            <input type="radio" class="radioInput" name="account_type" value="organizator" id="organizzator">
            <label for="Organizzator">Organizzatore</label>
        </div>
        <input type="button" class="submitButton" value="Registrati" />
    </form>
</body>

</html>