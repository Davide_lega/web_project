<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?php echo $templateParams["title"] ?></title>
    <script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="./js/overlay.js"></script>
    <script type="text/javascript" src="https://code.iconify.design/1/1.0.4/iconify.min.js"></script>

    <?php
    if (isset($templateParams["js"])) : //JS FUNCTIONS
        foreach ($templateParams["js"] as $script) :
    ?>
            <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php
        endforeach;
    endif;
    if (isset($templateParams["css"])) : //CSS STYLE
        foreach ($templateParams["css"] as $style) :
        ?>
            <link rel="stylesheet" href=<?php echo $style; ?> />
    <?php
        endforeach;
    endif;
    ?>
</head>

<body>
    <div class="overlay">
        <div class="menu-row">
            <h2 class="nav-title">Menu</h2>
            <button class="close-menu"><span class="iconify" data-icon="ic:outline-close" data-inline="false"></span></button>
            <?php if (isset($_SESSION['mail'])) : ?>
                <a class="logButton" href="./log_out.php" style="color: red;">
                    <span class="iconify" data-icon="ic:baseline-log-in" data-inline="false"></span>
                </a>
            <?php else : ?>
                <a class="logButton" href="./log_in.php">
                    <span class="iconify" data-icon="ic:baseline-log-in" data-inline="false"></span>
                </a>
            <?php endif; ?>

        </div>

        <div class="eventDisplay">
            <?php if (!empty($templateParams["user_event"])) :
                foreach ($templateParams["user_event"] as $user_event) :
            ?>
                    <div class="overlayEvent" id="overlayEvent">
                        <img class="overlayImage" src="<?php echo UPLOAD_DIR . $user_event["image"] ?>" />
                        <div class="overlayInfo">
                            <label class="overlayTitle" for="overlayEvent"><?php echo $user_event["title"] ?></label><br />
                            <div class="overlayColumnL">
                                <label class="overlayStartDate" for="overlayEvent">Dal: <?php echo $user_event["start_date_time"] ?></label><br />
                                <label class="overlayCity" for="overlayEvent">Città: <?php echo $user_event["city"] ?></label><br />
                            </div>
                            <div class="overlayColumnR">
                                <label class="overlayEndDate" for="overlayEvent">Al: <?php echo $user_event["end_date_time"] ?></label><br />
                                <label class="overlayTicket" for="overlayEvent">Numero di biglietti: <?php echo $user_event["quantity"] ?></label><br />
                            </div>
                        </div>
                    </div>
                <?php
                endforeach;
            else :
                if (isset($_SESSION['mail'])) :
                ?>
                    <div class="overlayNotEvent">
                        <br>
                        <label class="overlayMessage" for="overlayNotEvent">Acquista dei biglietti per visualizzarli qui.</label>
                    </div>
                <?php
                else :
                ?>
                    <div class="overlayNotEvent">
                        <br>
                        <label class="overlayMessage" for="overlayNotEvent">Accedi per vedere i tuoi eventi.</label>
                    </div>
            <?php
                endif;
            endif;
            ?>
        </div>
    </div>
    <header class="header">
        <?php
        $level = getAccountLevel();
        if ($level == 'admin') : ?>
            <a href="./index_admin.php"><img class="logo" src="upload/img/logo.png" alt="#" /></a>
        <?php else : ?>
            <a href="./index.php"><img class="logo" src="upload/img/logo.png" alt="#" /></a>
        <?php endif; ?>
        <h1 class="title">WOODLAND</h1>

        <div class="user-button">
            <a href="./cart.php" class="cart"><span class="iconify" data-icon="ion:cart-outline" data-inline="false"></span></a>
            <?php
            if (isset($_SESSION['mail'])) :
                $notifications = $dbh->getUserNotificationsToRead($_SESSION['mail']);
                $hasNotifications = count($notifications);
                if ($hasNotifications == true) : ?>
                    <a href="./notification.php" class="notification" style="color: red;"><span class="iconify" data-icon="ion:notifications-outline" data-inline="false"></span></a>
                <?php else : ?>
                    <a href="./notification.php" class="notification"><span class="iconify" data-icon="ion:notifications-outline" data-inline="false"></span></a>
                <?php endif; ?>
            <?php else : ?>
                <a href="./notification.php" class="notification"><span class="iconify" data-icon="ion:notifications-outline" data-inline="false"></span></a>
            <?php endif; ?>

            <button class="menu">
                <span class="iconify" id="shish" data-icon="bx:bx-user-circle" data-inline="false" onclick=""></span>
            </button>
        </div>

    </header>
    <main>
        <!--Load current page-->
        <?php
        if (isset($templateParams["page"])) {
            foreach ($templateParams["page"] as $page)
                require($page);
        }
        ?>
    </main>
    <footer class="infoSite">
        <div class="contacts">
            <h2>Contatti:</h2><br>
            <p>giammarco.amadori@studio.unibo.it</p>
            <p>davide.lega2@studio.unibo.it</p>
        </div>
        <div class="woodland">
            <h2>Woodland</h2><br>
            <a href="./term_and_conditions.php">
                <p>Chi siamo</p>
            </a>
            <a href="./term_and_conditions.php">
                <p>Termini e condizioni</p>
            </a>
            <a href="./term_and_conditions.php">
                <p>Privacy</p>
            </a>
            <a href="./term_and_conditions.php">
                <p>Cookie policy</p>
            </a>
        </div>
    </footer>
</body>

</html>