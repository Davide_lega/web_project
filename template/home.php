<div name="events">
    <!--events horizontal-->
</div>
<div class="baseHomeContent">
    <section class="slideShowSection">
        <div class="slideshow">
            <!--slideshow-->
            <a class="prevButton" onclick="prevSlide()">&#10094;</a>
            <a class="nextButton" onclick="nextSlide()">&#10095;</a>
            <a id="imgVisualA" href="">
                <img id="imgVisual" class="fade imgSlide" src="" />
            </a>
        </div><br />
        <div class="slidedot" id="slidedot">
            <!--dot-->
        </div>
    </section>
    <section class="infoDiscover">
        <h1 class="linkedTitle">Autore</h1>
        <p class="linkedDescription">Breve descrizione dell'evento</p>
        <div class="woodenExperience">
            <label for="">La tua "wooden experience" ti sta aspettando!</label>
            <a href="#">Registrati ora</a>
        </div>
        <div id="section">
            <h2>Scopri nuovi Wooders</h2>
            <div class="discoverDiv">
                <?php foreach ($templateParams["random_events1"] as $event) :  ?>
                    <a class="discoverButton" href="event.php?id=<?php echo $event["id_event"]; ?>">
                        <label class="discoverLabel" for="discoverHref"><?php echo $event["title"]; ?></label>
                        <img class="discoverImage" src="<?php echo UPLOAD_DIR . $event["image"]; ?>" alt="" /></a>
                <?php endforeach; ?>
                <?php foreach ($templateParams["random_events2"] as $event) :  ?>
                    <a class="discoverButton" href="event.php?id=<?php echo $event["id_event"]; ?>">
                        <label class="discoverLabel" for="discoverHref"><?php echo $event["title"]; ?></label>
                        <img class="discoverImage" src="<?php echo UPLOAD_DIR . $event["image"]; ?>" alt="" /></a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</div>
<div class="searchResultContent"></div>