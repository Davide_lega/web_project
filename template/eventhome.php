 <!--event image-->
 <div id="Title">
     <p><?php echo $event["0"]["title"] ?></p>
 </div>
 <div id="Img">
     <img class="EventImage" src="<?php echo UPLOAD_DIR . $event["0"]["image"]; ?>" alt="" />
 </div>
 <div id="Description">
     <p><?php echo $event["0"]["description"] ?></p>
     <br />
 </div>
 <section class="Infos">
     <div id="price">
         <p>Prezzo: <span><?php echo $event["0"]["price"] ?> €</span></p>
     </div>
     <div id="quantity">
         <p>Biglietti disponibili: <span><?php echo $event["0"]["quantity"] ?></span> </p>
     </div>
     <div id="date">
         <p>Dal:<span> <?php echo $event["0"]["start_date_time"] ?></span> al:<span> <?php echo $event["0"]["end_date_time"] ?></span></p>
     </div>
     <div id="city">
         <p>Città: <span><?php echo $event["0"]["city"] ?></span></p>
     </div>
     <div id="addButton">
         <?php
            $level = getAccountLevel();
            if ($level != 'organizator' && $event["0"]["quantity"] > 0) : ?>
             <button class="addButton" type="button" onclick="addEvent(<?php echo $_GET['id'] ?>, '<?php echo $_SESSION['mail'] ?>')">Aggiungi al carrello</button>
             <label for='addButton' class='checkLabel'>Aggiunto al carrello</label>
         <?php endif; ?>
     </div>
 </section>
 <!--LOGIN CHECK-->