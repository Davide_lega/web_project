 <form action="./template/process_eventUpdate.php" method="POST" id="form" enctype="multipart/form-data">
     <section class="secLeft secBase">
         <div id="eventTitle">
             <label class="eventLabel" for="title">Titolo:</label><br>
             <input type="text" name="title" class="inputTitle" value="<?php echo $event['0']['title'] ?>"></input>
         </div>
         <div id="eventPrice">
             <label class="eventLabel" for="price">Prezzo:</label><br>
             <input type="number" name="price" class="inputPrice" value="<?php echo $event['0']['price'] ?>" min="0"></input>
         </div>
         <div id="eventQuantity">
             <label class="eventLabel" for="quantity">Disponibilità:</label><br>
             <input type="number" name="quantity" class="inputQuantity" value="<?php echo $event['0']['quantity'] ?>" min="0"></input>
         </div>
         <div id="eventDate">
             <label class="eventLabel" for="startDate">Dal:</label><br>
             <input type="date" name="startDate" class="inputDateS" value="<?php echo $event['0']['start_date_time'] ?>" min="2020-01-01" max="3000-01-01"></input>
         </div>
         <div>
             <label class="eventLabel" for="endDate">Al: </label><br>
             <input type="date" name="endDate" class="inputDateE" value="<?php echo $event['0']['end_date_time'] ?>" min="2020-01-01" max="3000-01-01"></input>
         </div>
         <div id="eventCity">
             <label class="eventLabel" for="city">Location:</label><br>
             <input type="text" name="city" class="inputCity" value="<?php echo $event['0']['city'] ?>"></input>
         </div>
     </section>
     <section class="secRight secBase">
         <div id="eventImage">
             <div class="eventImageRadio">
                 <label for="eventImage">Immagine</label><br>
                 <label class="radioLabel" for="oldImage">
                     <input type="radio" name="imageRadio" id="oldImage" class="oldImage" value="old" checked="checked">Attuale</label>
                 <label class="radioLabel" for="newImage">
                     <input type="radio" name="imageRadio" id="newImage" class="newImage" value="new">Nuova</label>
             </div>
             <div class="eventImagedisplay">
                 <label class="imageLabel">Immagine attuale: </label>
                 <img class="image" src="<?php echo UPLOAD_DIR . $event["0"]["image"]; ?>" alt="" />
                 <input class="eventImageInput" type="file" name="newImage" id="newImage" accept="image/png, image/jpeg"></input>
             </div>



         </div>
         <div id="eventDescription">
             <label class="textLabel" for="desc">Descrizione: </label>
             <textarea name="desc" class="inputDesc"><?php echo $event['0']['description'] ?></textarea>
         </div>
     </section>
     <input type="hidden" name="id_event" class="id_event" value="<?php echo $event['0']['id_event'] ?>"></input>
     <input type="button" class="submit" value="Conferma" onclick="validateAndSubmit(<?php echo $_GET['id'] ?>)">
 </form>