<section class="displayOrgEvent">
    <?php $id = 1;
    foreach ($templateParams["myEvents"] as $event) :  ?>
        <div class="event">
            <a href="event_update.php?id=<?php echo $event["id_event"]; ?>"><img class="eventImage" src="<?php echo UPLOAD_DIR . $event["image"]; ?>" alt="" /></a>
            <button class="deleteEvent" id="<?php echo $id++ ?>"><span class=" iconify fixDelete" data-icon="ic:outline-close" data-inline="false"></span></button>
            <div class="eventInfo">
                <a href="event_update.php?id=<?php echo $event["id_event"]; ?>">
                    <h2 class="eventTitle" id="eventTitle"><?php echo $event["title"]; ?></h2>
                </a>
                <p class="eventP">Disponibilità: <?php echo $event["quantity"]; ?></p>
                <p class="eventP" id="eventCity">Città: <?php echo $event["city"]; ?></p>
                <p class="eventP">Categoria: <?php echo $event["category"]; ?></p>
                <p class="eventP" id="eventSDT">Inizio: <?php echo $event["start_date_time"]; ?></p>
                <p class="eventP" id="eventEDT">Fine: <?php echo $event["end_date_time"]; ?></p>
                <p class="eventP" id="eventId" style="display: none;"><?php echo $event["id_event"]; ?></p>
            </div>

        </div>

    <?php endforeach; ?>
</section>