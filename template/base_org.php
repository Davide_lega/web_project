<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?php echo $templateParams["title"] ?></title>
    <script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://code.iconify.design/1/1.0.4/iconify.min.js"></script>

    <?php
    if (isset($templateParams["js"])) : //JS FUNCTIONS
        foreach ($templateParams["js"] as $script) :
    ?>
            <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php
        endforeach;
    endif;
    if (isset($templateParams["css"])) : //CSS STYLE
        foreach ($templateParams["css"] as $style) :
        ?>
            <link rel="stylesheet" href=<?php echo $style; ?> />
    <?php
        endforeach;
    endif;
    ?>
</head>

<body>
    <header>
        <a href="./index_organizator.php"><img class="logo" src="upload/img/logo.png" alt="#" /></a>
        <h1 class="title">WOODLAND</h1>

        <div class="user-button">
            <a href="./event_creation.php" class="newEvent"><span class="iconify" data-icon="ant-design:file-add-outlined" data-inline="false"></span></span></a>
            <!--<a href="./notification.php" class="notification"><span class="iconify" data-icon="ion:notifications-outline" data-inline="false"></span></a>-->
            <a class="logButton" href="./log_out.php"><span class="iconify" data-icon="ic:baseline-log-in" data-inline="false"></span></a>
        </div>

    </header>
    <main>
        <!--Load current page-->
        <?php
        if (isset($templateParams["page"])) {
            foreach ($templateParams["page"] as $page)
                require($page);
        }
        ?>
    </main>
    <footer class="infoSite">
        <div class="contacts">
            <h2>Contatti:</h2><br>
            <p>giammarco.amadori@studio.unibo.it</p>
            <p>davide.lega2@studio.unibo.it</p>
        </div>
        <div class="woodland">
            <h2>Woodland</h2><br>
            <a href="./term_and_conditions.php">
                <p>Chi siamo</p>
            </a>
            <a href="./term_and_conditions.php">
                <p>Termini e condizioni</p>
            </a>
            <a href="./term_and_conditions.php">
                <p>Privacy</p>
            </a>
            <a href="./term_and_conditions.php">
                <p>Cookie policy</p>
            </a>
        </div>
    </footer>
</body>

</html>