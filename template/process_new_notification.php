<?php
$requireLogIn = false;
require_once '../bootstrap.php';

    if($_GET["NoType"]=="EventDelete"){
        $Ntext = "Caro utente le comunichiamo che l'evento ". $_SESSION["Etitle"] ." che si sarebbe dovuto svolgere in data ".
        $_SESSION["Start_date"]. " - ". $_SESSION["End_date"] ." e luogo : ". $_SESSION["Location"] 
        ." e' stato cancellato .";
        $Ntitle = "ATTENZIONE l'evento ". $_SESSION["Etitle"] ." e' stato cancellato.";
    }
    else if($_GET["NoType"]=="EventUpdate"){
        $Ntext = "Caro utente le comunichiamo che l'evento " . $_SESSION["Etitle"] . 
        " ha subito modifiche la invitiamo gentilmente a consultare la pagina dedicata.";
        $Ntitle = "Comuncazione di servizio l'evento ". $_SESSION["Etitle"] ." e' stato modificato .";
    }
    
    foreach ($_SESSION["Users"] as $val){

        $dbh->insertNotification(
            $val,
            $Ntext,
            $Ntitle
    );
    }    
    if($_SESSION["level"]=="admin"){
        header("Location: ../index_admin.php");
    }
    else if($_SESSION["level"]=="organizator"){
        header("Location: ../index_organizator.php");
    }
