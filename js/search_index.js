$(document).ready(function () {
	//Create the slider
	slider();
	//Create the multiInput on City and Category
	inputHandler();
	//Event click that begins the search
	$(".searchButton").click(function (event) {
		event.preventDefault();
		//Check consistency of input
		if (validate()) {
			//Create the empty array in the right order
			let selectorArray = { date: [], price: [], city: [], category: [] };

			//Cities parse input
			$("#formCity > .resizeInput").each(function () {
				if ($(this).val() != "") {
					selectorArray["city"].push($(this).val());
				}
			});
			//Categories parse input
			$("#formCategory > .resizeInput").each(function () {
				if ($(this).val() != "") {
					selectorArray["category"].push($(this).val());
				}
			});
			//Prices string manipulation and parse input
			prices = $("#amount").val();
			minPrice = prices.substring(0, 3);
			maxPrice = prices.substring(3, 9);
			minPrice = minPrice.replace("-", " ");
			minPrice = minPrice.replace("$", " ");
			minPrice = minPrice.trim();
			maxPrice = maxPrice.replace("-", " ");
			maxPrice = maxPrice.replace("$", " ");
			maxPrice = maxPrice.trim();
			selectorArray["price"].push(minPrice);
			selectorArray["price"].push(maxPrice);
			//Dates parse input and manipulation in the perspective of db
			tempSDate = "";
			tempEDate = "";
			//If a value is empty just put a prefixed value
			if ($(".formDateS").val() != "") {
				tempSDate = $(".formDateS").val();
			} else {
				tempSDate = "2001-09-11";
			}
			if ($(".formDateE").val() != "") {
				tempEDate = $(".formDateE").val();
			} else {
				tempEDate = "9999-12-12";
			}
			//THE "THING" BELOW IS DAVIDE'S FAULT (For date calculations)
			selectorArray["date"].push(tempSDate);
			selectorArray["date"].push(tempEDate);
			selectorArray["date"].push(tempSDate);
			selectorArray["date"].push(tempEDate);
			selectorArray["date"].push(tempSDate);
			selectorArray["date"].push(tempEDate);
			selectorArray["date"].push(tempSDate); //ODD
			selectorArray["date"].push(tempSDate);
			selectorArray["date"].push(tempEDate);
			selectorArray["date"].push(tempEDate); //ODD

			//API call
			$.get("./api/api-search.php", { selectorArray: selectorArray }, function (data) {
				eventArray = data;

				//Check if there are element if the returned array
				if (Object.values(eventArray).length == 0) {
					$(".searchResultContent").html("<label class='searchErrorLabel'>La tua ricerca non ha prodotto alcun risultato</label></br>");
					$(".searchResultContent").append("<button class='searchErrorResetButton' onclick='resetSearch()'>Chiudi</button>");
					$(".baseHomeContent").css("display", "none");
					$(".searchResultContent").css("display", "inline-block");
				} else {
					//Reset section
					$(".searchResultContent").html("<button class='searchResetButton' onclick='resetSearch()'>Chiudi</button>");
					$(".baseHomeContent").css("display", "none");
					//Print of each event, the string concatenation is to avoid unwanted tag closures
					$.each(eventArray, function (i, value) {
						$(".searchResultContent").css("display", "inline-block");
						event = "";
						event += "<div class='searchEvent' id='searchEvent'>";
						event += "<a href='event.php?id=" + value["id_event"] + "'>";
						event += "<img class='searchImage' src='./upload/img/" + value["image"] + "'/></a>";
						event += "<div class='searchInfo'>";
						event += "<a class='searchTitleA' href='event.php?id=" + value["id_event"] + "'>";
						event += "<label class='searchTitle' for='searchEvent'><b>" + value["title"] + "</b></label></a></br>";
						event += "<div class='searchColumnL'>";
						event += "<label class='searchStartDate' for='searchEvent'><b>Dal:</b> " + value["start_date_time"] + "</label></br>";
						event += "<label class='searchCity' for='searchEvent'><b>Città:</b> " + value["city"] + "</label></br>";
						event += "<label class='searchCategory' for='searchEvent'><b>Categoria:</b> " + value["category"] + "</label></div>";
						event += "<div class='searchColumnR'>";
						event += "<label class='searchEndDate' for='searchEvent'><b>Al:</b> " + value["end_date_time"] + "</label></br>";
						event += "<label class='searchTicket' for='searchEvent'><b>Biglietti disponibili:</b> " + value["quantity"] + "</label></br>";
						event += "<label class='searchPrice' for='searchEvent'><b>Prezzo:</b> " + value["price"] + "€</label></div>";
						event += "</div ></div >";
						$(".searchResultContent").append(event);
					});
				}
			}).fail(function () {
				console.log("Failed to get the event");
			});
		} else {
			alert("Please check your input");
		}
	});
});

//Reset the div for reseach
function resetSearch() {
	$(".searchResultContent").html("");
	$(".searchResultContent").css("display", "none");
	$(".baseHomeContent").css("display", "inline-block");
}

//Validate the input
function validate() {
	error = false;

	//Check if start date is before end date
	if ($("#formDateE").val() < $("#formDateS").val()) {
		$("#formDateS").css("color", "red");
		$("#formDateE").css("color", "red");
		error = true;
	} else {
		$("#formDateS").css("color", "black");
		$("#formDateE").css("color", "black");
	}
	return !error;
}

//Create the slider
function slider() {
	//Fixed value to avoid errors
	let dbMin = 0;
	let dbMax = 1;
	//Get max and min from db
	$.getJSON("./api/api-slider.php", function (data) {
		dbMin = data["MinPrice"];
		dbMax = data["MaxPrice"];
		//Create the slider
		$("#slider-range").slider({
			range: true,
			min: dbMin,
			max: dbMax,
			values: [dbMin, dbMax],
			slide: function (event, ui) {
				$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
			},
		});
		$("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
	});
}

//Creation and managing of the multiinputs
function inputHandler() {
	$(".multiInput").each(function () {
		//Create the input
		$(this).append(
			$("<input></input>").attr({
				class: "formInsideButton resizeInput",
				type: "text",
				value: "",
			})
		);
		//Attach the event listener responsible for the dimension of the input
		$(this).on("keyup", "input.resizeInput", function () {
			//Adjust the number by the font size
			let fontsize = 12.5;
			$(this).width($(this).val().length * fontsize);
		});
		//Add another input of focus in
		$(this).on("focusin", "input.resizeInput", function () {
			$(this)
				.parent()
				.append(
					$("<input></input>").attr({
						class: "formInsideButton resizeInput",
						type: "text",
						value: "",
					})
				);
		});
		//Remove the extra input on focus out
		$(this).on("focusout", "input.resizeInput", function () {
			$(".resizeInput:not(:last-child)").each(function () {
				if ($(this).val() == "") {
					$(this).remove();
				}
			});
		});
	});
}
