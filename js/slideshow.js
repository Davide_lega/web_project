//Some global variable
var slideArray = [];
var slideIndex = 0;
var NUMBER_ELEMENT = 3; //Start from 0
$(document).ready(function () {
    //Get the event info
	$.get("./api/api-slideshow.php", { numberOfElement: NUMBER_ELEMENT+1 }, function (data) {
		slideArray = data;
		slideIndex = 0;
        counter = 0;
        //Attach a dot to each event (counter is for current slide)
		$.each(slideArray, function () {
			let dotString = '<span class="dot" onclick="currentSlide(' + counter + ')" id=' + counter + "/></span>";
			$("div#slidedot").append(dotString);
			counter++;
        });
        //Start with image 0
		showSlides(slideIndex);
	});
});
//Go backward until the start and then go to the end
function prevSlide() {
	if (slideIndex == 0) {
		showSlides(NUMBER_ELEMENT);
	} else {
		showSlides(slideIndex-1);
	}
}
//Go forward until the end and then go to the start
function nextSlide() {
	if (slideIndex == NUMBER_ELEMENT) {
		showSlides(0);
	} else {
		showSlides(slideIndex+1);
	}
}
//Called from dots
function currentSlide(next) {
	showSlides(next);
}
//Change the image and the dot visualization from slideArray to next
function showSlides(next) {
    $("img#imgVisual").attr("src", slideArray[next]["image"]);
    $("a#imgVisualA").attr("href","event.php?id="+slideArray[next]["id_event"]); //Npt function
	$("div.slidedot > span#" + slideIndex).attr("class", "dot");
	$("div.slidedot > span#" + next).attr("class", "dot active");
	$("h1.linkedTitle").html(slideArray[next]["title"]);
	$("p.linkedDescription").html(slideArray[next]["description"]);
	slideIndex = next;
}
