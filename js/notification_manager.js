var notifications = [];

$(document).ready(function() {
  $.getJSON("./api/api-notification.php", function(data) {
    notifications = data;
    splitAndDraw(notifications);
  });
});

function splitAndDraw(notifications){
    let highPriorityNotifications = [];
    let LowPriorityNotifications = [];
    $.each(notifications, function(i, notification) {
        if(notification['readed'] == 0){
            highPriorityNotifications.push(notification);
        }
        else{
            LowPriorityNotifications.push(notification)
        }
    })
    drawNotification(highPriorityNotifications);
    drawNotification(LowPriorityNotifications);
}

function drawNotification(notifications) {
  
    $.each(notifications, function(i, notification) {
        $("section").append(
            $(`<div id ="${notification["notification_id"]}" class ="${notification["readed"]}"></div> `)
                .append($('<div class="NotificationTitle">')
                    .append($(`<p class="notificationTitle"> - ${notification["title"]} </p>`))
                    ) 
            .append($("<div class ='DeleteAndGoTo'>")
                .append($("<button type = button id = 'del' class='deleteEvent'>" 
                                + "<span class='iconify fixDelete' data-icon='ic:outline-close' data-inline='false'></span>" + "</button>").click(function() {
                                    deleteNotification(notification["notification_id"])
                                }))
                .append($("<button class='goTo deleteEvent'>"+ 
                "<span class='iconify fixDeleteL' data-icon='octicon:mail-read' data-inline='false'></span>"+"</button>").click(function() {
                    readSingleNotification(notification["notification_id"]);
                }) 
            )
        ));
    });

}

function readSingleNotification(id){
    
    $.post("./api/api-notification.php", { id_notification: id, op: "read" },function(){
        window.location.href = "./single_notification.php?id="+id;
    });
   
}

function deleteNotification(id) {
  $("div#" + id).remove();
  $.post("./api/api-notification.php", { id_notification: id, op: "delete" });
}