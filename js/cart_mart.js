var events = [];
$(document).ready(function () {
	$.getJSON("./api/api-cart.php", function (data) {
        events = data;
        if (Object.values(events).length == 0) {
            $("section").append("<label class='emptyCart' for='section'>Il tuo carrello è vuoto</label>");
            $(".Confirm").css("visibility","hidden")
        } else {
            drawEvents(events);
        }
	});
});

function drawEvents(events) {
	$.each(events, function (i, event) {
		$("section").append(
			$(`<div id ="${event["id_event"]}" class = "Ediv"/> `) // fare hide dopo
				.append(
					$(`<div class="titleAndCloseButton">`)
						.append($(`<p class="EventTitle"> - ${event["title"]} </p>`))
						.append(
							$(
								"<button type = button id='del' class='deleteEvent'>" +
									"<span class='iconify fixDelete' data-icon='ic:outline-close' data-inline='false'></span>" +
									"</button>"
							).click(function () {
								deleteEventfromCart(event["id_event"]);
							})
						)
				)
				.append(
					$("<div class='ticketArea'>")
						.append($(`<p> Numero Biglietti : </p>`))
						.append(
							$(
								"<button type = button id='dec'>" +
									"<span class='iconify' data-icon='ant-design:minus-square-outlined' data-inline='false'</span>" +
									"<button/>"
							).click(function () {
								decTicket(event["id_event"], event);
							})
						)
						.append($(`<p class ="tickets">${event["quantity"]} </p>`))
						.append(
							$(
								"<button type = button id='inc'>" +
									"<span class='iconify' data-icon='ant-design:plus-square-outlined' data-inline='false'></span>" +
									"<button/>"
							).click(function () {
								incTicket(event["id_event"], event);
							})
						)
						.append($(`<p class="errorLabel"> Non ci sono abbastanza biglietti disponibili </p>`))
				)
				.append($(`<p class="Pricetext"> Prezzo totale : </p>`).append($(`<p class="price">${event["price"] * event["quantity"]}</p>`)))
		);
	});
}
function deleteEventfromCart(id) {
	$("div#" + id).remove();
	$.post("./api/api-cart.php", { id_event: id, op: "delete" });
}
function incTicket(id, event) {
	$.get("./api/api-cart_check.php", { event: event }, function (inc) {
		if (inc === "true") {
			event["quantity"] = event["quantity"] + 1;
			$("div#" + id + "> div.ticketArea > p.tickets").text(" " + event["quantity"] + "");
			$("div#" + id + ">p.Pricetext>p.price").text(" " + event["quantity"] * event["price"] + "");
			$.post("./api/api-cart.php", { id: id, op: "+" });
		} else {
			$("div#" + id + "> button#inc").attr("disabled", true);
			$("div#" + id + "> div > p.errorLabel").css("display", "inline-block");
		}
	});
}

function decTicket(id, event) {
	if (event["quantity"] > 0) {
		event["quantity"] = event["quantity"] - 1;
		$("div#" + id + "> div.ticketArea > p.tickets").text(" " + event["quantity"] + "");
		$("div#" + id + ">p.Pricetext>p.price").text("" + event["quantity"] * event["price"] + "");
		$("div#" + id + "> button#inc").attr("disabled", false);
		$("div#" + id + "> div > p.errorLabel").css("display", "none");
		$.post("./api/api-cart.php", { id: id, op: "-" });
	}
}
function purchase() {
	window.location = "./template/process_purchased.php";
}
