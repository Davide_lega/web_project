$(document).ready(function() {
	$(".menu").on("click", function() {
		$(".overlay").addClass("open");
		$(".overlay").addClass("allowScroll");
		$("body").removeClass("allowScroll");
		$("body").addClass("blockScroll");
	});

	$(".close-menu").on("click", function() {
		$(".overlay").removeClass("open");
		$(".overlay").removeClass("allowScroll");
		$("body").removeClass("blockScroll");
		$("body").addClass("allowScroll");
	});
});
