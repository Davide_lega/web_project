$(document).ready(function () {
	$("#form :input").on("input selectionchange propertychange", function () {
		$(this).css("color", "#FFFFFF");
	});
	//Select new or old image
	$(".eventImageInput").hide();
	$(".image").show();
	$(".imageLabel").show();
	$("input[type=radio]").on("change", function (e) {
		var whichOne = $(e.currentTarget).val();
		if (whichOne == "new") {
			$(".image").hide();
			$(".imageLabel").hide();
			$(".eventImageInput").show();
		} else {
			$(".eventImageInput").hide();
			$(".image").show();
			$(".imageLabel").show();
		}
	});
});

function validateAndSubmit(id) {
	error = false;
	$("#form > input:not(input[type=file])").each(function () {
		//CHECK IF ALL INPUT HAVE VALUE
		$(this).css("border-color", "#707070");
		if ($(this).val() == "") {
			$(this).css("border-color", "red");
			error = true;
		}
    });
    
    $("input[type=file]").css("border-color", "#707070");    
    if ($("input[name=imageRadio]:checked").val() == "new" && $("input[type=file]").val() == "") {
		$("input[type=file]").css("border-color", "red");
		error = true;
	}
	if ($(".inputDateS").val() > $(".inputDateE").val()) {
		$(".inputDateS").css("border-color", "red");
		$(".inputDateE").css("border-color", "red");
		error = true;
	}
	//city to lower case
	$(".inputCity").val($(".inputCity").val().toLowerCase());
	if (!error) {
		$("#form").submit();
	}
}
