$(document).ready(function() {
	$(".submitButton").click(function(event) {
		error = false;
		event.preventDefault();

		$("#form :input").each(function() { //CHECK IF ALL INPUT HAVE VALUE
			$(this).css("border-color", "#707070");
			if ($(this).val() == "") {
				$(this).css("border-color", "red");
				error = true;
			}
		});
		if (!(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test($("#mail").val()))) {
			$("#mail").css("border-color", "red");
			error = true;
		}
		if (!error) {
			$("#form").append(
				$("<input></input>").attr({
					name: "passwordCript",
					type: "hidden",
					id: "passwordCript",
					value: hex_sha512($("#password").val())
				})
			);
			$("#password").val("");
			$("#form").submit();
		}
	});
});
