$(document).ready(function () {
	$(".deleteEvent").click(function (event) {
		let dataArray = { id: [], title: [], start_date_time: [], end_date_time: [], city: [] };
        id = $(this).attr("id");
        
		dataArray["id"] = $("#" + id + " + .eventInfo #eventId").html();
		dataArray["title"] = $("#" + id + " + .eventInfo #eventTitle").html();
		dataArray["start_date_time"] = $("#" + id + " + .eventInfo #eventSDT").html().replace("Inizio: ", "");
		dataArray["end_date_time"] = $("#" + id + " + .eventInfo #eventEDT").html().replace("Fine: ", "");
        dataArray["city"] = $("#" + id + " + .eventInfo #eventCity").html().replace("Città: ", "");
        
        window.location =
            "./template/process_organizator.php?id=" +
            dataArray["id"] +
            "&title=" +
            dataArray["title"] +
            "&start_date_time=" +
            dataArray["start_date_time"] +
            "&end_date_time=" +
            dataArray["end_date_time"] +
            "&city=" +
            dataArray["city"];
	});
});
