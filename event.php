<?php
$requireLogIn = true;
require_once 'bootstrap.php';

$event = $dbh->getEventById($_GET["id"]);
$templateParams["title"] = $event["0"]["title"];
$templateParams["js"] = array("./js/event_add.js");
$templateParams["css"] =array("./css/base.css","./css/event.css");
$templateParams["page"] = array("./template/eventhome.php");

if ($dbh->login_check()) {
    $templateParams["user_event"] = $dbh->getBuyedEventsByDate($_SESSION["mail"]);
}

require 'template/base.php';
?>