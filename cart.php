<?php
$requireLogIn = true;
require_once 'bootstrap.php';

$templateParams["js"] = array("./js/cart_mart.js");
$templateParams["css"] =array("./css/base.css","./css/cart.css");
$templateParams["page"] = array("./template/cart_template.php");

if ($dbh->login_check()) {
    $templateParams["user_event"] = $dbh->getBuyedEventsByDate($_SESSION["mail"]);
}

if(isset($_GET["error"])){
    if($_GET["error"]==1){
        $message = "Non è stato possibile acquistare uno o più biglietti";
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
}

require 'template/base.php';
?>