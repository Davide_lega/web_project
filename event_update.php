<?php
$requireLogIn = true;
require_once 'bootstrap.php';

$event = $dbh->getEventById($_GET["id"]);
$templateParams["title"] = "Modifica - " . $event["0"]["title"];
$templateParams["js"] = array("./js/eventUpdateValidator.js");
$templateParams["css"] = array("./css/base_org.css", "./css/event_update.css");
$templateParams["page"] = array("event_updateT.php");
//FINIRE IL CSS
require 'template/base_org.php';
