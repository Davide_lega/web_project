<?php
class DatabaseManager
{
    private $db;

    //DB CONSTRUCTOR
    public function __construct($servername, $dbusername, $dbpassword, $dbname)
    {
        $this->db = new mysqli($servername, $dbusername, $dbpassword, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }
    //SECURE LOGIN
    public function login($mail, $password)
    {
        if ($stmt = $this->db->prepare("SELECT mail, password_cript, salt, category FROM USER WHERE mail = ? LIMIT 1")) {
            $stmt->bind_param('s', $mail);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($mail, $db_password, $salt, $level);
            $stmt->fetch();
            $password = hash('sha512', $password . $salt);
            if ($stmt->num_rows == 1) { //User exist
                if ($db_password == $password) { //Login success
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    //$mail = preg_replace("/[^0-9]+/", "", $mail); // ci proteggiamo da un attacco XSS e invalido il login because reason
                    $_SESSION['mail'] = $mail;
                    $_SESSION['login_string'] = hash('sha512', $password . $user_browser);
                    $_SESSION['level'] = $level;
                    return true;
                } else { //Login failed
                    return false;
                }
            } else { //User doesn't exist
                return false;
            }
        }
    }
    //LOGIN CHECK BY SUPERGLOBAL VARIABLES
    public function login_check()
    {
        if (isset($_SESSION['mail'], $_SESSION['login_string'])) {
            $mail = $_SESSION['mail'];
            $login_string = $_SESSION['login_string'];
            $user_browser = $_SERVER['HTTP_USER_AGENT'];

            if ($stmt = $this->db->prepare("SELECT password_cript, salt FROM USER WHERE mail = ? LIMIT 1")) {
                $stmt->bind_param('s', $mail);
                $stmt->execute();
                $stmt->store_result();

                if ($stmt->num_rows == 1) { //If user exist
                    $stmt->bind_result($password, $salt);
                    $stmt->fetch();
                    $login_check = hash('sha512', $password . $user_browser);
                    if ($login_check == $login_string) {
                        return true; //Login successfull
                    }
                }
            }
        }
        return false; //Login failed
    }
    //UTILS FUNCTION
    public function is_logged()
    {
        return $this->login_check();
    }
    //INSERT INTO DB NEW USER
    public function new_User($mail, $password, $name, $surname, $postcode, $country, $category)
    {
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        $password = hash('sha512', $password . $random_salt);
        $query = "INSERT INTO USER (category, salt, country, password_cript, mail, postcode, surname, name)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        if ($stmt) {
            $stmt->bind_param('ssssssss', $category, $random_salt, $country, $password, $mail, $postcode, $surname, $name);
            $stmt->execute();
            return true;
        }
        return false;
    }
    //USER EXIST BY MAIL
    public function userExist($mail)
    {
        $stmt = $this->db->prepare("SELECT mail FROM USER WHERE mail = ?");
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET FROM DB 'n' RANDOM EVENT
    public function getRandomEvents($n)
    {
        $stmt = $this->db->prepare("SELECT id_event, title, image, description FROM EVENT ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET FROM DB EVENT BY 'id'
    public function getEventById($id)
    {
        $query = "SELECT price, id_event, title, quantity, image, description, start_date_time, end_date_time, city, category, mail
        FROM EVENT WHERE Event.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET FROM DB NOTIFICATIONS BY 'id'
    public function getNotificationsById($id)
    {
        $query = "SELECT readed, message, notification_id, title, mail
        FROM NOTIFICATION WHERE notification_id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET ALL EVENTS OF ORGANIZATOR 'mail'
    public function getOrganizatorEvents($mail)
    {
        $query = "SELECT id_event, title, image, start_date_time, end_date_time, city, category, quantity
        FROM EVENT WHERE EVENT.mail=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET ALL EVENT IN CART OF USER 'mail'
    public function getCartEvents($mail)
    {
        $query = "SELECT EVENT.price, EVENT.title, EVENT.id_event, CART.quantity, EVENT.city FROM EVENT, CART, USER WHERE CART.mail=?
        AND USER.mail = CART.mail AND EVENT.id_event = CART.id_event";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET USERS CART
    public function getUserCart($mail)
    {
        $query = "SELECT * FROM CART WHERE CART.mail=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET ALL BUYED EVENT OF USER 'mail'
    public function getBuyedEvents($mail)
    {
        $query = "SELECT EVENT.title, EVENT.image, EVENT.start_date_time, EVENT.end_date_time, EVENT.city, TICKET.quantity
        FROM EVENT, TICKET, USER WHERE USER.mail = ? AND TICKET.mail = USER.mail AND TICKET.id_EVENT = EVENT.id_EVENT";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTicket($mail, $id_event)
    {
        $query = "SELECT TICKET.id_event, TICKET.mail, TICKET.quantity 
        FROM TICKET WHERE TICKET.mail = ? AND TICKET.id_event =?";

        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $mail, $id_event);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET ALL ACTIVE EVENTS BETWEEN TWO DATES
    public function getBuyedEventsByDate($mail)
    {
        $query = "SELECT EVENT.title, EVENT.image, EVENT.start_date_time, EVENT.end_date_time, EVENT.city, TICKET.quantity
        FROM EVENT, TICKET, USER
        WHERE USER.mail = ? AND TICKET.mail = USER.mail AND TICKET.id_EVENT = EVENT.id_EVENT ORDER BY start_date_time ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //SELECT ALL EVENTS BY SELECTOR FROM INDEX (ho pianto scrivendola)
    public function getEventByAllSelector($selectorArray)
    {
        $paramsArray = array();
        $bindType = "";
        $query = "SELECT EVENT.price, EVENT.id_event, EVENT.title, EVENT.image, EVENT.description, EVENT.start_date_time, EVENT.end_date_time,
        EVENT.city, EVENT.category, EVENT.quantity FROM EVENT WHERE EVENT.id_event = id_event";
        if (!empty($selectorArray)) {
            if (isset($selectorArray['date'])) {
                $query .= " AND (( start_date_time < ? AND end_date_time > ? ) OR ( start_date_time >= ? AND end_date_time <= ? )
                OR ( start_date_time < ? AND end_date_time <= ? AND end_date_time >= ?) OR ( start_date_time >= ? AND start_date_time <= ? AND end_date_time >= ? ))";
                $bindType .= "ssssssssss"; //10s
            }
            if (isset($selectorArray['price'])) {
                $query .= " AND ( price >= ? AND price <= ? )";
                $bindType .= "ii";
            }
            if (isset($selectorArray["city"])) {
                $elements = count($selectorArray["city"]);
                for ($i = 0; $i < $elements; $i++) {
                    if ($i == 0) {
                        $query .= " AND (EVENT.city = ?";
                    } else {
                        $query .= " OR EVENT.city = ?";
                    }
                    $bindType .= "s";
                }
                $query .= ")";
            }
            if (isset($selectorArray["category"])) {
                $elements = count($selectorArray["category"]);
                for ($i = 0; $i < $elements; $i++) {
                    if ($i == 0) {
                        $query .= " AND (EVENT.category = ?";
                    } else {
                        $query .= " OR EVENT.category = ?";
                    }
                    $bindType .= "s";
                }
                $query .= ")";
            }
            foreach ($selectorArray as $value) { //MAYBE THROW AN ERROR

                foreach ($value as $elem) {
                    if (!empty($value) && !empty($elem)) {
                        array_push($paramsArray, $elem);
                    }
                }
            }
        }

        $stmt = $this->db->prepare($query);
        $stmt->bind_param($bindType, ...$paramsArray);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    //GET ALL NOTIFICATIONS OF USER 'mail'
    public function getUserNotifications($mail)
    {
        $query = "SELECT readed, message, notification_id, title, mail
        FROM NOTIFICATION WHERE NOTIFICATION.mail = ? ORDER BY notification_id DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getUserNotificationsToRead($mail){
        $query = "SELECT readed, message, notification_id, title, mail
        FROM NOTIFICATION WHERE NOTIFICATION.mail = ? AND readed = 0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //GET ALL USERS THAT BUYED A TICKET FOR AN EVENT
    public function getUserThatBuyedEvent($idEvent)
    {
        $query = "SELECT mail FROM TICKET WHERE TICKET.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idEvent);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getUserThatHaveEventOnCart($idEvent)
    {
        $query = "SELECT mail FROM CART WHERE CART.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idEvent);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getUsersToNotify($idEvent){

        $BuyesEventsPpl=$this->getUserThatBuyedEvent($idEvent);
 
        $CartEventsPpl=$this->getUserThatHaveEventOnCart($idEvent);

        return array_unique(array_merge(array_column($BuyesEventsPpl,"mail"),array_column($CartEventsPpl,"mail")));
    }
    //INSERT TO USER 'client_mail' CART NEW TICKET(S)
    public function insertCart($quantity, $client_mail, $id_event)
    {
        $query = "INSERT INTO CART (quantity,mail,id_event) VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isi', $quantity, $client_mail, $id_event);
        $stmt->execute();
        return $stmt->insert_id; // cosa fa sta riga?
    }
    //INSERT NEW NOTIFICATION
    public function insertNotification($mail, $message, $title)
    {
        $query = "INSERT INTO NOTIFICATION (readed, mail, message, title) VALUES (0, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $mail, $message, $title);
        $stmt->execute();

        return $stmt->insert_id; // cosa fa sta riga?
    }
    //INSERT NEW EVENT
    public function insertEvent($quantity, $price, $end_date_time, $start_date_time, $image, $description, $title, $city, $category, $mail)
    {
        $query = "INSERT INTO EVENT (quantity, price, end_date_time, start_date_time, image, description, title, city, category, mail)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iissssssss', $quantity, $price, $end_date_time, $start_date_time, $image, $description, $title, $city, $category, $mail);
        $stmt->execute();
        return $stmt->insert_id; //RETURN THE ID OF THE NEW EVENT
    }
    //INSERT NEW TICKET(S)
    public function insertTicket($mail, $id_event, $quantity)
    {
        $query = "INSERT INTO TICKET (mail, id_event, quantity) VALUES(?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssi', $mail, $id_event, $quantity);
        $stmt->execute();
        return $stmt->insert_id; //RETURN THE ID OF THE NEW EVENT
    }
    //UPDATE QUANTITY IN USER 'mail' CART
    public function updateQuantityInCart($quantity, $mail, $id_event)
    {
        $query = "UPDATE CART SET quantity = ? WHERE mail = ? AND EVENT.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isi', $quantity, $mail, $id_event);
        return $stmt->execute();
    }
    //QUANTITY OF 'id_event' +1 IN CART
    public function updatePlusInCart($mail, $id_event)
    {
        $query = "UPDATE CART SET quantity = (SELECT quantity FROM CART WHERE CART.mail=? AND CART.id_event = ?) + 1 
        WHERE mail = ? AND id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sisi', $mail, $id_event, $mail, $id_event);
        return $stmt->execute();
    }
    //QUANTITY OF 'id_event' -1 IN CART
    public function updateMinusInCart($mail, $id_event)
    {
        $query = "UPDATE CART SET quantity = (SELECT quantity FROM CART WHERE CART.mail=? AND CART.id_event = ?) - 1 
        WHERE mail = ? AND id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sisi', $mail, $id_event, $mail, $id_event);
        return $stmt->execute();
    }
    //UPDATE SOMETHING OF A CERTAIN EVENT
    public function updateEvent($title, $city, $quantity, $price, $end_date_time, $start_date_time, $image, $description, $id_event)
    {
        $query = "UPDATE EVENT SET title = ?, city = ?, quantity = ?, price = ?, end_date_time = ?, start_date_time = ?, image = ?, description = ?
         WHERE EVENT.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssiissssi', $title, $city, $quantity, $price, $end_date_time, $start_date_time, $image, $description, $id_event);
        return $stmt->execute();
    }
    public function updateEventWithoutImage($title, $city, $quantity, $price, $end_date_time, $start_date_time, $description, $id_event)
    {
        $query = "UPDATE EVENT SET title = ?, city = ?, quantity = ?, price = ?, end_date_time = ?, start_date_time = ?, description = ?
         WHERE EVENT.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssiisssi', $title, $city, $quantity, $price, $end_date_time, $start_date_time, $description, $id_event);
        return $stmt->execute();
    }
    //UPDATE ONLY TICKET QUANTITY OF A CERTAIN EVENT
    public function updateEventQuantity($id_event, $quantity)
    {
        $query = "UPDATE EVENT SET quantity = ? WHERE EVENT.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $quantity, $id_event);
        return $stmt->execute();
    }
    //UPDATE ONLY HOW MUCH TICKET I BUYED
    public function updateTicketQuantity($id_event, $mail, $quantity)
    {
        $query = "UPDATE TICKET SET quantity = ? WHERE TICKET.id_event = ? AND TICKET.mail = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iis', $quantity, $id_event, $mail);
        return $stmt->execute();
    }
    //CHANGE A NOTIFICATION TO READ
    public function updateNotificationToRead($notification_id)
    {
        $query = "UPDATE NOTIFICATION SET readed = 1 WHERE notification_id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $notification_id);
        return $stmt->execute();
    }
    //DELETE USER 'mail' FROM DB
    public function deleteUSER($mail)
    {
        $query = "DELETE FROM USER WHERE USER.mail = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $mail);
        $stmt->execute();
        if($stmt->affected_rows>=1){
            return true;
        }
        return false;
    }
    //DELETE EVENT 'id_event' FROM DB
    public function deleteEVENT($id_event)
    {
        $query = "DELETE FROM EVENT WHERE id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id_event);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->affected_rows>=1){
            return true;
        }
        return false;
    }
    //DELETE EVENT 'id_event' FROM USER 'mail' CART
    public function deleteFromCART($mail, $id_event)
    {
        $query = "DELETE FROM CART WHERE  CART.mail = ? AND CART.id_event = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $mail, $id_event);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }
    //DELETE A NOTIFICATION
    public function deleteNOTIFICATIONS($notification_id)
    {
        $query = "DELETE FROM NOTIFICATION WHERE  notification_id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $notification_id);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function getMaxPrice()
    {
        $query = "SELECT MAX(price) AS MaxPrice FROM EVENT";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getMinPrice()
    {
        $query = "SELECT MIN(price) AS MinPrice FROM EVENT";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
