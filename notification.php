<?php
$requireLogIn = true;
require_once 'bootstrap.php';
$templateParams["title"] = "Notifications";
$templateParams["js"] = array("./js/notification_manager.js");
$templateParams["css"] =array("./css/base.css","./css/notification.css");
$templateParams["page"] = array("./template/notification_template.php");

if ($dbh->login_check()) {
    $templateParams["user_event"] = $dbh->getBuyedEventsByDate($_SESSION["mail"]);
}
require 'template/base.php';    

?>