<?php
$requireLogIn = false;
require_once 'bootstrap.php';

$templateParams["title"] = "Log In";
$templateParams["js"] = array("./js/sha512.js", "./js/form_login.js");
$templateParams["css"] = array("./css/log_in.css");

//ERROR MANAGEMENT
if(isset($_GET["error"])){
    if($_GET["error"]==1){
        $templateParams["error"] = "Mail o password non corretti";
    } else if ($_GET["error"] == 2) {
        $templateParams["error"] = "Mail o password non inseriti correttamente";
    } else if ($_GET["error"] == 3) {
        $templateParams["error"] = "Login del Sign Up fallito";
    }
}

//REDIRECT MANAGEMENT

require 'template/log_inT.php';
